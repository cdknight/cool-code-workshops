---
marp: true
---

# Git
## How to Manage Your Programming Projects

---

# What is Git?

* Git is known as a **version control system**, which is used to manage revisions of code projects
* A code project using Git is known as a **git repository** 
* There are websites to host your git repositories so you can access it from anywhere and collaborate with others.
* Popular code-hosting websites include **GitHub**, **GitLab**, and **Bitbucket**.

---

# How Does Git Work?

* Git primarily uses **branches** and **commits** to keep track of your code.
* **Branches** are for different concurrently existing versions of the project: you could have a different branch for version 1.0 and version 2.0 to keep the separate versions updated, for example.
  * You could also use branches for the development and the deployment (version ready for use) versions of the software.
* **Commits** are logged updates to your code, that are part of a **branch**. 

---

# Understanding Basic Command Line Commands

* We will need this to work with the Git CLI
* Computers store files in **directories** (aka folders), as you probably already know. A fast way to work with directories is with the command line, where programmers do a lot of work.
* The simple commands for navigating through your disk are:
   * On Windows: `dir` and `cd` 
      * `dir` will list the files and directories in the current directory
      * `cd` will change your directory to a desired directory
    * On Linux/macOS `ls` and `cd`
      * `ls` does the same thing as `dir` but it has more features that we won't delve into here(sorry Windows users)
      * `cd` does the same thing as on Windows

---

# Adding Git to Your First Project

* We need to make a Git **repository** first. A repository is basically just your project of code.
* Run `git init .` to initialize a new repository in the directory with your code.
* Now, we want to add all the project files (all the files and folders in the current directory) to git. To do this, we run `git add .`. If we wanted to only add a specific file, we could do `git add filename`.
* To remove files (if we accidentally added them, for example), we can do `git rm filename`

---

# Commiting and Pushing Our Changes

* Now we need to **commit** our changes (added and removed files).
* To do this, we run `git commit -m "message with change description here"`
   * The `-m` allows us to specify a commit message detailing what changes we've made to our code since our previous commit (this doesn't have to be a super long and detailed paragraph).
* The next step is to push our changes to whatever website we desire.

---

# Commiting and Pushing Our Changes

<style scoped>
    img {
        height: 300px;
    }

</style>

* We need to upload our code to a code hosting site like GitHub.

If you create a repository in GitHub follow the instructions with the `git remote add origin`:



![GitHub](github.resized.png )

---

# Pushing Your Code

* To upload your code, you can just run `git push origin master`, which will uplaod your code to whatever code-hosting platform you chose.
* To commit code in the future, you can just run `git add .`, `git commit -m "message"`, and then `git push origin master`. You typically do not have to change the remote.

