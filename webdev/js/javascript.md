---
marp: true
---

<style scoped>

section {
    background-color: orange;
}

h1 {
    color: #0b2b3d;  
    text-align: center;
    background-color: white;
    padding: 40px;
    border: 10px solid black;
    border-radius: 20px;
    font-size: 70px;
    
}
</style>


# Getting Started with JavaScript 



---

# What is JavaScript?

* Programming language used to create **interactivity** on websites.
   * Example: click a button and update a counter

* Can run on the client or the server

* Used to make **single-page-applications** - uses JavaScript to update the information that is currently on the page
   * Faster webapges

---

# Including JS in HTML

* Javascript is added **inline**, **in a tag**, or **in an external file**
  * Just like CSS
  * Mostly written in tags and external files.
* To add JS to a tag use `<script>`
* To import JS from a separate file use `<script src="path-to-file.js"></script>`

--- 

# Your First Taste of JS

This mixes JavaScript and HTML together.
```html
<button onclick="increment()">Increment!</button>
<p id="counter">0</p>

<script>
var counter = 0;

function increment() {
    counter += 1;
    document.getElementById("counter").innerHTML = counter;
}

</script>
```

--- 

# Understanding JavaScript - HTML

```html 
<button onclick="increment()">Increment!</button>
```
We create a button here - this button will display "Increment!" and every time it is clicked it will run the function `increment()` as defined in the `onclick` attribute.

```html
<p id="counter">0</p>
```
We create a paragraph element here to display text - in this case, the number of times the button was clicked. Note that we assign this `<p>` an `id`. This is so we can reference it in JavaScript.

---

# Understanding JavaScript - The Code

Now let's take a look at the JavaScript itself.
```js
var counter = 0;
```
* We create a counter variable. 
* JavaScript doesn't need variable types, it can automatically determine the type.

```js
function increment() {
    counter += 1;
    document.getElementById("counter").innerHTML = counter;
}
```

* The function can access our variables that are outside the function (`counter`). 
* First, we increment the counter. 

--- 

# Understanding JavaScript - The Code (Continued)

* Then, we use the `document.getElementById` function to get the `<p>` element. The object this function returns has a property `innerHTML`, which is simply what goes between the tags in HTML. We set this to the new value of the counter.

---

# Try It!

* Open the `increment.html` file that you edited from the file manager.

* Try pressing `Increment!` a few times!

* You'll see somethng like this: 

![Increment Screenshot](increment-screenshot.png)