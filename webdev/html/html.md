---
marp: true
---

# My First Website

In this workshop, you'll learn the essentials of creating a website, terms and jargon associated with creating a website, and by the end you'll have your very own website!

---

## Some Essential Terms

Throughout your journey of web development, you'll end up meeting a lot of terms that you might be unfamiliar of. Here are some definitions to help you out.

- **Backend:** This is a piece of some web applications that connects the frontend to code that stores/gets information and performs more complex logic. This is typically written in a language like Node.JS, Python, or maybe Java.

- **Frontend:** The part of the website that runs on the user's web browser (like Chrome) and has the visual and interactive elements (CSS, JS, HTML)

---

# Essential Terms Continued

- **HTML:** This stands for HyperText Markup Language and is what you use in order to define the structure of a website.

- **CSS:** This stands for Cascading Style Sheets and is what you use _style_ your website; it is to change how your website looks.

- **JS:** This is often just referred to as JavaScript, and it is used to make your website more interactive.

---

### The Structure of an HTML File

1. Open VSCode, make a new folder, and open a new file called `index.html`
2. Write all this code by **hand**.

Let's take a look at what this does. 
```html
<!DOCTYPE html>
<html>
    <head>
        <title>My first website!</title>
    </head>

    <body>
        <h1>Hello World!</h1>
    </body>

</html>
```

---

# The Structure of an HTML Page

- We start the webpage with the `<!DOCTYPE html>`. 
  - You don't need to worry about this too much, but it simply tells web browsers that we are using HTML5, which is the latest version of HTML.

- Next is the `<html>` tag. What is a tag?
  - A tag is like the basic building block of an HTML document, defining structure and storing information that will be used to render the web page.
  - Each tag is closed by a tag that has a slash before the tag name, like `</html>` for the `<html>` tag. 
  - Note how the `<html>` tag has more tags in it. This tag defines the beginning of the HTML document where you put all the web page information.


---

# Structure (Continued)

- The `<head>` tag **does not** contain any visual elements of the webpage. 
   - It is simply for data about the webpage, along with adding JS and CSS files to the HTML page.

- The `<body>` tag houses all the visual elements. 
  - If you want to add something to your page, then this is _probably_ the place to do it .
  - Inside the `<body>` we stumble upon the `<h1>` tag which is a big heading. 

--- 

# HTML Tags

There are `h` elements from 1-6, 1 being the largest and 6 being the smallest. For example, `<h4>` is smaller than `<h3>`.

But there are many more elements than just `<h1>` to ,`<h6>` - for all of them you can perhaps visit [this link](https://www.w3schools.com/TAGs/), but we will be going over a few on the next slide.

---

# HTML Tags (Continued)

There are quite a few HTML tags, but here are a few.

1. The `<div>` tag (used to make sections in your code)
1. The `<h1>` to `<h6>` tags.
2. The `<p>` tag. - Used for general text (like this!)
3. The `<img>`tag. - Used to add images to your website 
4. The `<code>` tag. - Used to add code blocks to your website.

As we saw before, the content typically goes between the opening of the tag and the closing of the tag `<h1>(here)</h1>`, but there are some exceptions to this, such as the `<img>` tag: to add an image `<img src="source-of-image">` would be what you want. The `src=` defines an **attribute**, in this case telling us where the image is located.


---

# Closing Out with HTML

This is just the tip of the iceberg. Again, you can consult w3schools for all sorts of tags and HTML-related tutorials.
 






