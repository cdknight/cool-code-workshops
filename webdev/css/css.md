---
marp: true
---

# Getting Started with CSS
## Make your webpages look fancy!

---

# What is CSS?

* CSS, or Cascading Style Sheets is the "style sheet language" used to change the appearance of HTML web pages.
* CSS can handle different styles for desktop websites and mobile websites.
* CSS will style elements for simple events (like hovering over elements)

---

# How Does CSS Work with HTML?

* CSS can be included in an HTML webpage in ways like JavaScript, if you know JavaScript already

---

# How Do I Include CSS in My HTML?

* You can include it **inline**, in a separate tag (the `<style>` tag), and in a separate file.
* We are going to primarily use the `<style>` tag in this tutorial.


--- 

# How To Write CSS

* CSS will select certain tags/elements and apply a specific style to them.
* We can use **classes**, **ID**s, and tag names to select specific elements to style.
* **Classes** use a `.` before the name of the class to select them in CSS.
   * For example, `.large-sections` would be a sample CSS class selector.
* **ID** use a `#` before the ID name
  * For example, `#title` would be a sample CSS ID selector.
* You can also select all tags of a type, like `h1` to select all `<h1>` tags and style them.

---

# CSS Properties

* **CSS properties** are used to change the style of the elements we selected earlier.

Let's take a look at some sample code.

```css
h1 {
    font-family: "Verdana", sans-serif;
}
```

* This means that we set the font for all `h1` tags to the Verdana font.

* There are many, many, many CSS properties. 

---

# CSS Properties (continued)

Here are some commonly-used CSS properties:
* `color` (changes the color of text)
* `font-family`, `font-size`, and `font-weight`
* `margin`
* `padding` 
* `border` (ex. `border: 1px solid black`)
* `background-color`(sets the background color for the document or a div)





